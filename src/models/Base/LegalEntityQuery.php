<?php

namespace samizdam\econ\models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use samizdam\econ\models\LegalEntity as ChildLegalEntity;
use samizdam\econ\models\LegalEntityQuery as ChildLegalEntityQuery;
use samizdam\econ\models\Map\LegalEntityTableMap;

/**
 * Base class that represents a query for the 'LegalEntity' table.
 *
 *
 *
 * @method     ChildLegalEntityQuery orderById($order = Criteria::ASC) Order by the id column
 * @method     ChildLegalEntityQuery orderByTitle($order = Criteria::ASC) Order by the title column
 * @method     ChildLegalEntityQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildLegalEntityQuery orderByCreatedAt($order = Criteria::ASC) Order by the created_at column
 * @method     ChildLegalEntityQuery orderByUpdatedAt($order = Criteria::ASC) Order by the updated_at column
 *
 * @method     ChildLegalEntityQuery groupById() Group by the id column
 * @method     ChildLegalEntityQuery groupByTitle() Group by the title column
 * @method     ChildLegalEntityQuery groupByType() Group by the type column
 * @method     ChildLegalEntityQuery groupByCreatedAt() Group by the created_at column
 * @method     ChildLegalEntityQuery groupByUpdatedAt() Group by the updated_at column
 *
 * @method     ChildLegalEntityQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLegalEntityQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLegalEntityQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLegalEntityQuery leftJoinLegalEntityType($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalEntityType relation
 * @method     ChildLegalEntityQuery rightJoinLegalEntityType($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalEntityType relation
 * @method     ChildLegalEntityQuery innerJoinLegalEntityType($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalEntityType relation
 *
 * @method     ChildLegalEntityQuery leftJoinBank($relationAlias = null) Adds a LEFT JOIN clause to the query using the Bank relation
 * @method     ChildLegalEntityQuery rightJoinBank($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Bank relation
 * @method     ChildLegalEntityQuery innerJoinBank($relationAlias = null) Adds a INNER JOIN clause to the query using the Bank relation
 *
 * @method     ChildLegalEntityQuery leftJoinPlant($relationAlias = null) Adds a LEFT JOIN clause to the query using the Plant relation
 * @method     ChildLegalEntityQuery rightJoinPlant($relationAlias = null) Adds a RIGHT JOIN clause to the query using the Plant relation
 * @method     ChildLegalEntityQuery innerJoinPlant($relationAlias = null) Adds a INNER JOIN clause to the query using the Plant relation
 *
 * @method     ChildLegalEntityQuery leftJoinLegalEntityHasAccount($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalEntityHasAccount relation
 * @method     ChildLegalEntityQuery rightJoinLegalEntityHasAccount($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalEntityHasAccount relation
 * @method     ChildLegalEntityQuery innerJoinLegalEntityHasAccount($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalEntityHasAccount relation
 *
 * @method     \samizdam\econ\models\LegalEntityTypeQuery|\samizdam\econ\models\BankQuery|\samizdam\econ\models\PlantQuery|\samizdam\econ\models\LegalEntityHasAccountQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLegalEntity findOne(ConnectionInterface $con = null) Return the first ChildLegalEntity matching the query
 * @method     ChildLegalEntity findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLegalEntity matching the query, or a new ChildLegalEntity object populated from the query conditions when no match is found
 *
 * @method     ChildLegalEntity findOneById(int $id) Return the first ChildLegalEntity filtered by the id column
 * @method     ChildLegalEntity findOneByTitle(string $title) Return the first ChildLegalEntity filtered by the title column
 * @method     ChildLegalEntity findOneByType(string $type) Return the first ChildLegalEntity filtered by the type column
 * @method     ChildLegalEntity findOneByCreatedAt(string $created_at) Return the first ChildLegalEntity filtered by the created_at column
 * @method     ChildLegalEntity findOneByUpdatedAt(string $updated_at) Return the first ChildLegalEntity filtered by the updated_at column *

 * @method     ChildLegalEntity requirePk($key, ConnectionInterface $con = null) Return the ChildLegalEntity by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntity requireOne(ConnectionInterface $con = null) Return the first ChildLegalEntity matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalEntity requireOneById(int $id) Return the first ChildLegalEntity filtered by the id column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntity requireOneByTitle(string $title) Return the first ChildLegalEntity filtered by the title column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntity requireOneByType(string $type) Return the first ChildLegalEntity filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntity requireOneByCreatedAt(string $created_at) Return the first ChildLegalEntity filtered by the created_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntity requireOneByUpdatedAt(string $updated_at) Return the first ChildLegalEntity filtered by the updated_at column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalEntity[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLegalEntity objects based on current ModelCriteria
 * @method     ChildLegalEntity[]|ObjectCollection findById(int $id) Return ChildLegalEntity objects filtered by the id column
 * @method     ChildLegalEntity[]|ObjectCollection findByTitle(string $title) Return ChildLegalEntity objects filtered by the title column
 * @method     ChildLegalEntity[]|ObjectCollection findByType(string $type) Return ChildLegalEntity objects filtered by the type column
 * @method     ChildLegalEntity[]|ObjectCollection findByCreatedAt(string $created_at) Return ChildLegalEntity objects filtered by the created_at column
 * @method     ChildLegalEntity[]|ObjectCollection findByUpdatedAt(string $updated_at) Return ChildLegalEntity objects filtered by the updated_at column
 * @method     ChildLegalEntity[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LegalEntityQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \samizdam\econ\models\Base\LegalEntityQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'main', $modelName = '\\samizdam\\econ\\models\\LegalEntity', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLegalEntityQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLegalEntityQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLegalEntityQuery) {
            return $criteria;
        }
        $query = new ChildLegalEntityQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLegalEntity|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LegalEntityTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LegalEntityTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalEntity A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT id, title, type, created_at, updated_at FROM LegalEntity WHERE id = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_INT);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLegalEntity $obj */
            $obj = new ChildLegalEntity();
            $obj->hydrate($row);
            LegalEntityTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLegalEntity|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LegalEntityTableMap::COL_ID, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LegalEntityTableMap::COL_ID, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the id column
     *
     * Example usage:
     * <code>
     * $query->filterById(1234); // WHERE id = 1234
     * $query->filterById(array(12, 34)); // WHERE id IN (12, 34)
     * $query->filterById(array('min' => 12)); // WHERE id > 12
     * </code>
     *
     * @param     mixed $id The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterById($id = null, $comparison = null)
    {
        if (is_array($id)) {
            $useMinMax = false;
            if (isset($id['min'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_ID, $id['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($id['max'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_ID, $id['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalEntityTableMap::COL_ID, $id, $comparison);
    }

    /**
     * Filter the query on the title column
     *
     * Example usage:
     * <code>
     * $query->filterByTitle('fooValue');   // WHERE title = 'fooValue'
     * $query->filterByTitle('%fooValue%'); // WHERE title LIKE '%fooValue%'
     * </code>
     *
     * @param     string $title The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByTitle($title = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($title)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $title)) {
                $title = str_replace('*', '%', $title);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LegalEntityTableMap::COL_TITLE, $title, $comparison);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LegalEntityTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the created_at column
     *
     * Example usage:
     * <code>
     * $query->filterByCreatedAt('2011-03-14'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt('now'); // WHERE created_at = '2011-03-14'
     * $query->filterByCreatedAt(array('max' => 'yesterday')); // WHERE created_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $createdAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByCreatedAt($createdAt = null, $comparison = null)
    {
        if (is_array($createdAt)) {
            $useMinMax = false;
            if (isset($createdAt['min'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_CREATED_AT, $createdAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($createdAt['max'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_CREATED_AT, $createdAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalEntityTableMap::COL_CREATED_AT, $createdAt, $comparison);
    }

    /**
     * Filter the query on the updated_at column
     *
     * Example usage:
     * <code>
     * $query->filterByUpdatedAt('2011-03-14'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt('now'); // WHERE updated_at = '2011-03-14'
     * $query->filterByUpdatedAt(array('max' => 'yesterday')); // WHERE updated_at > '2011-03-13'
     * </code>
     *
     * @param     mixed $updatedAt The value to use as filter.
     *              Values can be integers (unix timestamps), DateTime objects, or strings.
     *              Empty strings are treated as NULL.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByUpdatedAt($updatedAt = null, $comparison = null)
    {
        if (is_array($updatedAt)) {
            $useMinMax = false;
            if (isset($updatedAt['min'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_UPDATED_AT, $updatedAt['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($updatedAt['max'])) {
                $this->addUsingAlias(LegalEntityTableMap::COL_UPDATED_AT, $updatedAt['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalEntityTableMap::COL_UPDATED_AT, $updatedAt, $comparison);
    }

    /**
     * Filter the query by a related \samizdam\econ\models\LegalEntityType object
     *
     * @param \samizdam\econ\models\LegalEntityType|ObjectCollection $legalEntityType The related object(s) to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByLegalEntityType($legalEntityType, $comparison = null)
    {
        if ($legalEntityType instanceof \samizdam\econ\models\LegalEntityType) {
            return $this
                ->addUsingAlias(LegalEntityTableMap::COL_TYPE, $legalEntityType->getType(), $comparison);
        } elseif ($legalEntityType instanceof ObjectCollection) {
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }

            return $this
                ->addUsingAlias(LegalEntityTableMap::COL_TYPE, $legalEntityType->toKeyValue('PrimaryKey', 'Type'), $comparison);
        } else {
            throw new PropelException('filterByLegalEntityType() only accepts arguments of type \samizdam\econ\models\LegalEntityType or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalEntityType relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function joinLegalEntityType($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalEntityType');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalEntityType');
        }

        return $this;
    }

    /**
     * Use the LegalEntityType relation LegalEntityType object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \samizdam\econ\models\LegalEntityTypeQuery A secondary query class using the current class as primary query
     */
    public function useLegalEntityTypeQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLegalEntityType($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalEntityType', '\samizdam\econ\models\LegalEntityTypeQuery');
    }

    /**
     * Filter the query by a related \samizdam\econ\models\Bank object
     *
     * @param \samizdam\econ\models\Bank|ObjectCollection $bank the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByBank($bank, $comparison = null)
    {
        if ($bank instanceof \samizdam\econ\models\Bank) {
            return $this
                ->addUsingAlias(LegalEntityTableMap::COL_ID, $bank->getId(), $comparison);
        } elseif ($bank instanceof ObjectCollection) {
            return $this
                ->useBankQuery()
                ->filterByPrimaryKeys($bank->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByBank() only accepts arguments of type \samizdam\econ\models\Bank or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Bank relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function joinBank($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Bank');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Bank');
        }

        return $this;
    }

    /**
     * Use the Bank relation Bank object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \samizdam\econ\models\BankQuery A secondary query class using the current class as primary query
     */
    public function useBankQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinBank($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Bank', '\samizdam\econ\models\BankQuery');
    }

    /**
     * Filter the query by a related \samizdam\econ\models\Plant object
     *
     * @param \samizdam\econ\models\Plant|ObjectCollection $plant the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByPlant($plant, $comparison = null)
    {
        if ($plant instanceof \samizdam\econ\models\Plant) {
            return $this
                ->addUsingAlias(LegalEntityTableMap::COL_ID, $plant->getId(), $comparison);
        } elseif ($plant instanceof ObjectCollection) {
            return $this
                ->usePlantQuery()
                ->filterByPrimaryKeys($plant->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByPlant() only accepts arguments of type \samizdam\econ\models\Plant or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the Plant relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function joinPlant($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('Plant');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'Plant');
        }

        return $this;
    }

    /**
     * Use the Plant relation Plant object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \samizdam\econ\models\PlantQuery A secondary query class using the current class as primary query
     */
    public function usePlantQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinPlant($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'Plant', '\samizdam\econ\models\PlantQuery');
    }

    /**
     * Filter the query by a related \samizdam\econ\models\LegalEntityHasAccount object
     *
     * @param \samizdam\econ\models\LegalEntityHasAccount|ObjectCollection $legalEntityHasAccount the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLegalEntityQuery The current query, for fluid interface
     */
    public function filterByLegalEntityHasAccount($legalEntityHasAccount, $comparison = null)
    {
        if ($legalEntityHasAccount instanceof \samizdam\econ\models\LegalEntityHasAccount) {
            return $this
                ->addUsingAlias(LegalEntityTableMap::COL_ID, $legalEntityHasAccount->getLegalEntityId(), $comparison);
        } elseif ($legalEntityHasAccount instanceof ObjectCollection) {
            return $this
                ->useLegalEntityHasAccountQuery()
                ->filterByPrimaryKeys($legalEntityHasAccount->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLegalEntityHasAccount() only accepts arguments of type \samizdam\econ\models\LegalEntityHasAccount or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalEntityHasAccount relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function joinLegalEntityHasAccount($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalEntityHasAccount');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalEntityHasAccount');
        }

        return $this;
    }

    /**
     * Use the LegalEntityHasAccount relation LegalEntityHasAccount object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \samizdam\econ\models\LegalEntityHasAccountQuery A secondary query class using the current class as primary query
     */
    public function useLegalEntityHasAccountQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLegalEntityHasAccount($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalEntityHasAccount', '\samizdam\econ\models\LegalEntityHasAccountQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLegalEntity $legalEntity Object to remove from the list of results
     *
     * @return $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function prune($legalEntity = null)
    {
        if ($legalEntity) {
            $this->addUsingAlias(LegalEntityTableMap::COL_ID, $legalEntity->getId(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the LegalEntity table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalEntityTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LegalEntityTableMap::clearInstancePool();
            LegalEntityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalEntityTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LegalEntityTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LegalEntityTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LegalEntityTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    // timestampable behavior

    /**
     * Filter by the latest updated
     *
     * @param      int $nbDays Maximum age of the latest update in days
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function recentlyUpdated($nbDays = 7)
    {
        return $this->addUsingAlias(LegalEntityTableMap::COL_UPDATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by update date desc
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function lastUpdatedFirst()
    {
        return $this->addDescendingOrderByColumn(LegalEntityTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by update date asc
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function firstUpdatedFirst()
    {
        return $this->addAscendingOrderByColumn(LegalEntityTableMap::COL_UPDATED_AT);
    }

    /**
     * Order by create date desc
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function lastCreatedFirst()
    {
        return $this->addDescendingOrderByColumn(LegalEntityTableMap::COL_CREATED_AT);
    }

    /**
     * Filter by the latest created
     *
     * @param      int $nbDays Maximum age of in days
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function recentlyCreated($nbDays = 7)
    {
        return $this->addUsingAlias(LegalEntityTableMap::COL_CREATED_AT, time() - $nbDays * 24 * 60 * 60, Criteria::GREATER_EQUAL);
    }

    /**
     * Order by create date asc
     *
     * @return     $this|ChildLegalEntityQuery The current query, for fluid interface
     */
    public function firstCreatedFirst()
    {
        return $this->addAscendingOrderByColumn(LegalEntityTableMap::COL_CREATED_AT);
    }

} // LegalEntityQuery
