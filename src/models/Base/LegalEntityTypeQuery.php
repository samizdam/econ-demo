<?php

namespace samizdam\econ\models\Base;

use \Exception;
use \PDO;
use Propel\Runtime\Propel;
use Propel\Runtime\ActiveQuery\Criteria;
use Propel\Runtime\ActiveQuery\ModelCriteria;
use Propel\Runtime\ActiveQuery\ModelJoin;
use Propel\Runtime\Collection\ObjectCollection;
use Propel\Runtime\Connection\ConnectionInterface;
use Propel\Runtime\Exception\PropelException;
use samizdam\econ\models\LegalEntityType as ChildLegalEntityType;
use samizdam\econ\models\LegalEntityTypeQuery as ChildLegalEntityTypeQuery;
use samizdam\econ\models\Map\LegalEntityTypeTableMap;

/**
 * Base class that represents a query for the 'LegalEntityType' table.
 *
 *
 *
 * @method     ChildLegalEntityTypeQuery orderByType($order = Criteria::ASC) Order by the type column
 * @method     ChildLegalEntityTypeQuery orderByBaseTax($order = Criteria::ASC) Order by the baseTax column
 *
 * @method     ChildLegalEntityTypeQuery groupByType() Group by the type column
 * @method     ChildLegalEntityTypeQuery groupByBaseTax() Group by the baseTax column
 *
 * @method     ChildLegalEntityTypeQuery leftJoin($relation) Adds a LEFT JOIN clause to the query
 * @method     ChildLegalEntityTypeQuery rightJoin($relation) Adds a RIGHT JOIN clause to the query
 * @method     ChildLegalEntityTypeQuery innerJoin($relation) Adds a INNER JOIN clause to the query
 *
 * @method     ChildLegalEntityTypeQuery leftJoinLegalEntity($relationAlias = null) Adds a LEFT JOIN clause to the query using the LegalEntity relation
 * @method     ChildLegalEntityTypeQuery rightJoinLegalEntity($relationAlias = null) Adds a RIGHT JOIN clause to the query using the LegalEntity relation
 * @method     ChildLegalEntityTypeQuery innerJoinLegalEntity($relationAlias = null) Adds a INNER JOIN clause to the query using the LegalEntity relation
 *
 * @method     \samizdam\econ\models\LegalEntityQuery endUse() Finalizes a secondary criteria and merges it with its primary Criteria
 *
 * @method     ChildLegalEntityType findOne(ConnectionInterface $con = null) Return the first ChildLegalEntityType matching the query
 * @method     ChildLegalEntityType findOneOrCreate(ConnectionInterface $con = null) Return the first ChildLegalEntityType matching the query, or a new ChildLegalEntityType object populated from the query conditions when no match is found
 *
 * @method     ChildLegalEntityType findOneByType(string $type) Return the first ChildLegalEntityType filtered by the type column
 * @method     ChildLegalEntityType findOneByBaseTax(int $baseTax) Return the first ChildLegalEntityType filtered by the baseTax column *

 * @method     ChildLegalEntityType requirePk($key, ConnectionInterface $con = null) Return the ChildLegalEntityType by primary key and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntityType requireOne(ConnectionInterface $con = null) Return the first ChildLegalEntityType matching the query and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalEntityType requireOneByType(string $type) Return the first ChildLegalEntityType filtered by the type column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 * @method     ChildLegalEntityType requireOneByBaseTax(int $baseTax) Return the first ChildLegalEntityType filtered by the baseTax column and throws \Propel\Runtime\Exception\EntityNotFoundException when not found
 *
 * @method     ChildLegalEntityType[]|ObjectCollection find(ConnectionInterface $con = null) Return ChildLegalEntityType objects based on current ModelCriteria
 * @method     ChildLegalEntityType[]|ObjectCollection findByType(string $type) Return ChildLegalEntityType objects filtered by the type column
 * @method     ChildLegalEntityType[]|ObjectCollection findByBaseTax(int $baseTax) Return ChildLegalEntityType objects filtered by the baseTax column
 * @method     ChildLegalEntityType[]|\Propel\Runtime\Util\PropelModelPager paginate($page = 1, $maxPerPage = 10, ConnectionInterface $con = null) Issue a SELECT query based on the current ModelCriteria and uses a page and a maximum number of results per page to compute an offset and a limit
 *
 */
abstract class LegalEntityTypeQuery extends ModelCriteria
{
    protected $entityNotFoundExceptionClass = '\\Propel\\Runtime\\Exception\\EntityNotFoundException';

    /**
     * Initializes internal state of \samizdam\econ\models\Base\LegalEntityTypeQuery object.
     *
     * @param     string $dbName The database name
     * @param     string $modelName The phpName of a model, e.g. 'Book'
     * @param     string $modelAlias The alias for the model in this query, e.g. 'b'
     */
    public function __construct($dbName = 'main', $modelName = '\\samizdam\\econ\\models\\LegalEntityType', $modelAlias = null)
    {
        parent::__construct($dbName, $modelName, $modelAlias);
    }

    /**
     * Returns a new ChildLegalEntityTypeQuery object.
     *
     * @param     string $modelAlias The alias of a model in the query
     * @param     Criteria $criteria Optional Criteria to build the query from
     *
     * @return ChildLegalEntityTypeQuery
     */
    public static function create($modelAlias = null, Criteria $criteria = null)
    {
        if ($criteria instanceof ChildLegalEntityTypeQuery) {
            return $criteria;
        }
        $query = new ChildLegalEntityTypeQuery();
        if (null !== $modelAlias) {
            $query->setModelAlias($modelAlias);
        }
        if ($criteria instanceof Criteria) {
            $query->mergeWith($criteria);
        }

        return $query;
    }

    /**
     * Find object by primary key.
     * Propel uses the instance pool to skip the database if the object exists.
     * Go fast if the query is untouched.
     *
     * <code>
     * $obj  = $c->findPk(12, $con);
     * </code>
     *
     * @param mixed $key Primary key to use for the query
     * @param ConnectionInterface $con an optional connection object
     *
     * @return ChildLegalEntityType|array|mixed the result, formatted by the current formatter
     */
    public function findPk($key, ConnectionInterface $con = null)
    {
        if ($key === null) {
            return null;
        }
        if ((null !== ($obj = LegalEntityTypeTableMap::getInstanceFromPool((string) $key))) && !$this->formatter) {
            // the object is already in the instance pool
            return $obj;
        }
        if ($con === null) {
            $con = Propel::getServiceContainer()->getReadConnection(LegalEntityTypeTableMap::DATABASE_NAME);
        }
        $this->basePreSelect($con);
        if ($this->formatter || $this->modelAlias || $this->with || $this->select
         || $this->selectColumns || $this->asColumns || $this->selectModifiers
         || $this->map || $this->having || $this->joins) {
            return $this->findPkComplex($key, $con);
        } else {
            return $this->findPkSimple($key, $con);
        }
    }

    /**
     * Find object by primary key using raw SQL to go fast.
     * Bypass doSelect() and the object formatter by using generated code.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @throws \Propel\Runtime\Exception\PropelException
     *
     * @return ChildLegalEntityType A model object, or null if the key is not found
     */
    protected function findPkSimple($key, ConnectionInterface $con)
    {
        $sql = 'SELECT type, baseTax FROM LegalEntityType WHERE type = :p0';
        try {
            $stmt = $con->prepare($sql);
            $stmt->bindValue(':p0', $key, PDO::PARAM_STR);
            $stmt->execute();
        } catch (Exception $e) {
            Propel::log($e->getMessage(), Propel::LOG_ERR);
            throw new PropelException(sprintf('Unable to execute SELECT statement [%s]', $sql), 0, $e);
        }
        $obj = null;
        if ($row = $stmt->fetch(\PDO::FETCH_NUM)) {
            /** @var ChildLegalEntityType $obj */
            $obj = new ChildLegalEntityType();
            $obj->hydrate($row);
            LegalEntityTypeTableMap::addInstanceToPool($obj, (string) $key);
        }
        $stmt->closeCursor();

        return $obj;
    }

    /**
     * Find object by primary key.
     *
     * @param     mixed $key Primary key to use for the query
     * @param     ConnectionInterface $con A connection object
     *
     * @return ChildLegalEntityType|array|mixed the result, formatted by the current formatter
     */
    protected function findPkComplex($key, ConnectionInterface $con)
    {
        // As the query uses a PK condition, no limit(1) is necessary.
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKey($key)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->formatOne($dataFetcher);
    }

    /**
     * Find objects by primary key
     * <code>
     * $objs = $c->findPks(array(12, 56, 832), $con);
     * </code>
     * @param     array $keys Primary keys to use for the query
     * @param     ConnectionInterface $con an optional connection object
     *
     * @return ObjectCollection|array|mixed the list of results, formatted by the current formatter
     */
    public function findPks($keys, ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getReadConnection($this->getDbName());
        }
        $this->basePreSelect($con);
        $criteria = $this->isKeepQuery() ? clone $this : $this;
        $dataFetcher = $criteria
            ->filterByPrimaryKeys($keys)
            ->doSelect($con);

        return $criteria->getFormatter()->init($criteria)->format($dataFetcher);
    }

    /**
     * Filter the query by primary key
     *
     * @param     mixed $key Primary key to use for the query
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKey($key)
    {

        return $this->addUsingAlias(LegalEntityTypeTableMap::COL_TYPE, $key, Criteria::EQUAL);
    }

    /**
     * Filter the query by a list of primary keys
     *
     * @param     array $keys The list of primary key to use for the query
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function filterByPrimaryKeys($keys)
    {

        return $this->addUsingAlias(LegalEntityTypeTableMap::COL_TYPE, $keys, Criteria::IN);
    }

    /**
     * Filter the query on the type column
     *
     * Example usage:
     * <code>
     * $query->filterByType('fooValue');   // WHERE type = 'fooValue'
     * $query->filterByType('%fooValue%'); // WHERE type LIKE '%fooValue%'
     * </code>
     *
     * @param     string $type The value to use as filter.
     *              Accepts wildcards (* and % trigger a LIKE)
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function filterByType($type = null, $comparison = null)
    {
        if (null === $comparison) {
            if (is_array($type)) {
                $comparison = Criteria::IN;
            } elseif (preg_match('/[\%\*]/', $type)) {
                $type = str_replace('*', '%', $type);
                $comparison = Criteria::LIKE;
            }
        }

        return $this->addUsingAlias(LegalEntityTypeTableMap::COL_TYPE, $type, $comparison);
    }

    /**
     * Filter the query on the baseTax column
     *
     * Example usage:
     * <code>
     * $query->filterByBaseTax(1234); // WHERE baseTax = 1234
     * $query->filterByBaseTax(array(12, 34)); // WHERE baseTax IN (12, 34)
     * $query->filterByBaseTax(array('min' => 12)); // WHERE baseTax > 12
     * </code>
     *
     * @param     mixed $baseTax The value to use as filter.
     *              Use scalar values for equality.
     *              Use array values for in_array() equivalent.
     *              Use associative array('min' => $minValue, 'max' => $maxValue) for intervals.
     * @param     string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function filterByBaseTax($baseTax = null, $comparison = null)
    {
        if (is_array($baseTax)) {
            $useMinMax = false;
            if (isset($baseTax['min'])) {
                $this->addUsingAlias(LegalEntityTypeTableMap::COL_BASETAX, $baseTax['min'], Criteria::GREATER_EQUAL);
                $useMinMax = true;
            }
            if (isset($baseTax['max'])) {
                $this->addUsingAlias(LegalEntityTypeTableMap::COL_BASETAX, $baseTax['max'], Criteria::LESS_EQUAL);
                $useMinMax = true;
            }
            if ($useMinMax) {
                return $this;
            }
            if (null === $comparison) {
                $comparison = Criteria::IN;
            }
        }

        return $this->addUsingAlias(LegalEntityTypeTableMap::COL_BASETAX, $baseTax, $comparison);
    }

    /**
     * Filter the query by a related \samizdam\econ\models\LegalEntity object
     *
     * @param \samizdam\econ\models\LegalEntity|ObjectCollection $legalEntity the related object to use as filter
     * @param string $comparison Operator to use for the column comparison, defaults to Criteria::EQUAL
     *
     * @return ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function filterByLegalEntity($legalEntity, $comparison = null)
    {
        if ($legalEntity instanceof \samizdam\econ\models\LegalEntity) {
            return $this
                ->addUsingAlias(LegalEntityTypeTableMap::COL_TYPE, $legalEntity->getType(), $comparison);
        } elseif ($legalEntity instanceof ObjectCollection) {
            return $this
                ->useLegalEntityQuery()
                ->filterByPrimaryKeys($legalEntity->getPrimaryKeys())
                ->endUse();
        } else {
            throw new PropelException('filterByLegalEntity() only accepts arguments of type \samizdam\econ\models\LegalEntity or Collection');
        }
    }

    /**
     * Adds a JOIN clause to the query using the LegalEntity relation
     *
     * @param     string $relationAlias optional alias for the relation
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function joinLegalEntity($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        $tableMap = $this->getTableMap();
        $relationMap = $tableMap->getRelation('LegalEntity');

        // create a ModelJoin object for this join
        $join = new ModelJoin();
        $join->setJoinType($joinType);
        $join->setRelationMap($relationMap, $this->useAliasInSQL ? $this->getModelAlias() : null, $relationAlias);
        if ($previousJoin = $this->getPreviousJoin()) {
            $join->setPreviousJoin($previousJoin);
        }

        // add the ModelJoin to the current object
        if ($relationAlias) {
            $this->addAlias($relationAlias, $relationMap->getRightTable()->getName());
            $this->addJoinObject($join, $relationAlias);
        } else {
            $this->addJoinObject($join, 'LegalEntity');
        }

        return $this;
    }

    /**
     * Use the LegalEntity relation LegalEntity object
     *
     * @see useQuery()
     *
     * @param     string $relationAlias optional alias for the relation,
     *                                   to be used as main alias in the secondary query
     * @param     string $joinType Accepted values are null, 'left join', 'right join', 'inner join'
     *
     * @return \samizdam\econ\models\LegalEntityQuery A secondary query class using the current class as primary query
     */
    public function useLegalEntityQuery($relationAlias = null, $joinType = Criteria::INNER_JOIN)
    {
        return $this
            ->joinLegalEntity($relationAlias, $joinType)
            ->useQuery($relationAlias ? $relationAlias : 'LegalEntity', '\samizdam\econ\models\LegalEntityQuery');
    }

    /**
     * Exclude object from result
     *
     * @param   ChildLegalEntityType $legalEntityType Object to remove from the list of results
     *
     * @return $this|ChildLegalEntityTypeQuery The current query, for fluid interface
     */
    public function prune($legalEntityType = null)
    {
        if ($legalEntityType) {
            $this->addUsingAlias(LegalEntityTypeTableMap::COL_TYPE, $legalEntityType->getType(), Criteria::NOT_EQUAL);
        }

        return $this;
    }

    /**
     * Deletes all rows from the LegalEntityType table.
     *
     * @param ConnectionInterface $con the connection to use
     * @return int The number of affected rows (if supported by underlying database driver).
     */
    public function doDeleteAll(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalEntityTypeTableMap::DATABASE_NAME);
        }

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con) {
            $affectedRows = 0; // initialize var to track total num of affected rows
            $affectedRows += parent::doDeleteAll($con);
            // Because this db requires some delete cascade/set null emulation, we have to
            // clear the cached instance *after* the emulation has happened (since
            // instances get re-added by the select statement contained therein).
            LegalEntityTypeTableMap::clearInstancePool();
            LegalEntityTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

    /**
     * Performs a DELETE on the database based on the current ModelCriteria
     *
     * @param ConnectionInterface $con the connection to use
     * @return int             The number of affected rows (if supported by underlying database driver).  This includes CASCADE-related rows
     *                         if supported by native driver or if emulated using Propel.
     * @throws PropelException Any exceptions caught during processing will be
     *                         rethrown wrapped into a PropelException.
     */
    public function delete(ConnectionInterface $con = null)
    {
        if (null === $con) {
            $con = Propel::getServiceContainer()->getWriteConnection(LegalEntityTypeTableMap::DATABASE_NAME);
        }

        $criteria = $this;

        // Set the correct dbName
        $criteria->setDbName(LegalEntityTypeTableMap::DATABASE_NAME);

        // use transaction because $criteria could contain info
        // for more than one table or we could emulating ON DELETE CASCADE, etc.
        return $con->transaction(function () use ($con, $criteria) {
            $affectedRows = 0; // initialize var to track total num of affected rows

            LegalEntityTypeTableMap::removeInstanceFromPool($criteria);

            $affectedRows += ModelCriteria::delete($con);
            LegalEntityTypeTableMap::clearRelatedInstancePool();

            return $affectedRows;
        });
    }

} // LegalEntityTypeQuery
