<?php
namespace samizdam\econ\models;

use samizdam\econ\models\Base\LegalEntityType as BaseLegalEntityType;

/**
 * Skeleton subclass for representing a row from the 'LegalEntityType' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements. This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class LegalEntityType extends BaseLegalEntityType
{

    const TYPE_BANK = 'Bank';

    const TYPE_PLANT = 'Plant';
}
