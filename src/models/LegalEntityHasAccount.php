<?php

namespace samizdam\econ\models;

use samizdam\econ\models\Base\LegalEntityHasAccount as BaseLegalEntityHasAccount;

/**
 * Skeleton subclass for representing a row from the 'LegalEntityHasAccount' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class LegalEntityHasAccount extends BaseLegalEntityHasAccount
{

}
