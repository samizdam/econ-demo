<?php
namespace samizdam\econ\models;

use samizdam\econ\models\Base\Bank as BaseBank;

/**
 * Skeleton subclass for representing a row from the 'Bank' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements. This class will only be generated as
 * long as it does not already exist in the output directory.
 */
class Bank extends BaseBank
{

    const LEGAL_ENTITY_TYPE = LegalEntityType::TYPE_BANK;

    public function __construct()
    {
        $this->setType(self::LEGAL_ENTITY_TYPE);
    }
}
