<?php

namespace samizdam\econ\models;

use samizdam\econ\models\Base\Plant as BasePlant;

/**
 * Skeleton subclass for representing a row from the 'Plant' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Plant extends BasePlant
{

    const LEGAL_ENTITY_TYPE = LegalEntityType::TYPE_PLANT;
    
    public function __construct()
    {
        $this->setType(self::LEGAL_ENTITY_TYPE);
    }
}
