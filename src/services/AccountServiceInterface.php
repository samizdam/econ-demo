<?php
namespace samizdam\econ\services;

use samizdam\econ\models\Bank;
use samizdam\econ\models\LegalEntity;
use samizdam\econ\models\Currency;

interface AccountServiceInterface
{

    public function openAccount(Bank $bank, LegalEntity $legalEntity, Currency $currency);
}