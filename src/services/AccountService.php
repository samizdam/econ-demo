<?php
namespace samizdam\econ\services;

use samizdam\econ\models\Bank;
use samizdam\econ\models\LegalEntity;
use samizdam\econ\models\Currency;
use samizdam\econ\models\Account;
use samizdam\econ\models\LegalEntityHasAccount;

class AccountService implements AccountServiceInterface
{
    /**
     * (non-PHPdoc)
     * @see \samizdam\econ\services\AccountServiceInterface::openAccount()
     * @return Account
     */
    public function openAccount(Bank $bank, LegalEntity $legalEntity, Currency $currency)
    {
        $account = new Account();
        $account->setBank($bank);
        $account->setCurrency($currency);
        
        $link = new LegalEntityHasAccount();
        $link->setAccount($account);
        $link->setLegalEntity($legalEntity);

        $account->save();
        return $account;
    }
}