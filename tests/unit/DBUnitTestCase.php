<?php
namespace samizdam\econ;

abstract class DBUnitTestCase extends \PHPUnit_Extensions_Database_TestCase
{

    protected function tearDown()
    {
        $this->clearPropelInstancePool();
        return parent::tearDown();
    }

    /**
     * After cleaning tables via DBUnit methods, deleted item exist in pool
     */
    private function clearPropelInstancePool()
    {
        $modelNamespace = "samizdam\\econ\\models\\";
        
        foreach ($this->getDataSet()->getTableNames() as $tableName) {
            $modelName = $modelNamespace . $tableName;
            call_user_func([
                $modelName::TABLE_MAP,
                'clearInstancePool'
            ]);
        }
    }

    protected function getTearDownOperation()
    {
        return \PHPUnit_Extensions_Database_Operation_Factory::TRUNCATE();
    }

    protected function getConnection()
    {
        $pdo = \Propel\Runtime\Propel::getServiceContainer()->getConnection()->getWrappedConnection();
        return $this->createDefaultDBConnection($pdo, $pdo->getName());
    }

    protected function getDataSet()
    {
        return $this->createArrayDataSet([]);
    }
}