<?php
namespace samizdam\econ\services;

use samizdam\econ\DBUnitTestCase;
use samizdam\econ\models\BankQuery;
use samizdam\econ\models\LegalEntityQuery;
use samizdam\econ\models\CurrencyQuery;
use samizdam\econ\models\Account;

class AccountServiceTest extends DBUnitTestCase
{

    protected function getDataSet()
    {
        return $this->createArrayDataSet([
            'LegalEntity' => [
                [
                    'id' => 1,
                    'type' => 'Bank'
                ],
                [
                    'id' => 2,
                    'type' => 'Plant'
                ]
            ],
            'Bank' => [
                [
                    'id' => 1
                ]
            ],
            'Plant' => [
                [
                    'id' => 2
                ]
            ],
            'Account' => [],
            'LegalEntityHasAccount' => []
        ]);
    }

    public function testOpenAccount()
    {
        $service = new AccountService();
        /* @var $bank \samizdam\econ\models\Bank */
        $bank = BankQuery::create()->findOneById(1);
        /* @var $legalEntity \samizdam\econ\models\LegalEntity */
        $legalEntity = LegalEntityQuery::create()->findOneById(2);
        $currency = CurrencyQuery::create()->findOneByCode('ECON');
        
        $this->assertTableRowCount('Account', 0);
        $this->assertEquals(0, $bank->countAccounts());
        
        $account = $service->openAccount($bank, $legalEntity, $currency);
        
        $this->assertInstanceOf(Account::class, $account);
        $this->assertTableRowCount('Account', 1);
        $this->assertEquals(1, $bank->countAccounts());
        $this->assertEquals(0, $account->getAmount());
        $this->assertEquals($currency, $account->getCurrency());
    }
}