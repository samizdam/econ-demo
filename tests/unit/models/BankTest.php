<?php
namespace samizdam\econ\models;

use samizdam\econ\DBUnitTestCase;

class BankTest extends DBUnitTestCase
{
    
    protected function getDataSet(){
        return $this->createArrayDataSet([
            'Bank' => [],
            'LegalEntity' => [],
        ]);
    }
    
    public function testSave()
    {
        $this->assertTableRowCount('Bank', 0);
        $this->assertTableRowCount('LegalEntity', 0);
        
        $bank = new Bank();
        $bank->save();

        $this->assertEquals($bank->getLegalEntity()
            ->getId(), $bank->getId());
        
        $legalEntity = LegalEntityQuery::create()->findOneById($bank->getId());
        $this->assertEquals($legalEntity->getBank(), $bank);
        
        $this->assertTableRowCount('Bank', 1);
        $this->assertTableRowCount('LegalEntity', 1);        
        
        $existedBank = BankQuery::create()->findOneById($bank->getId());
        $this->assertEquals($existedBank, $bank);
        
    }
}