<?php
namespace samizdam\econ\models;

use samizdam\econ\DBUnitTestCase;

class PlantTest extends DBUnitTestCase
{

    protected function getDataSet(){
        return $this->createArrayDataSet([
            'Plant' => [],
            'LegalEntity' => [],
        ]);
    }
    
    public function testSave()
    {
        $this->assertTableRowCount('Plant', 0);
        $this->assertTableRowCount('LegalEntity', 0);
        
        $plant = new Plant();
        $plant->save();

        $this->assertTableRowCount('Plant', 1);
        $this->assertTableRowCount('LegalEntity', 1);
        
        $this->assertNull($plant->getLegalEntity()
            ->getBank());
        $this->assertNotNull($plant->getLegalEntity()
            ->getPlant());
        
    }
}