<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1437855293.
 * Generated on 2015-07-25 23:14:53 
 */
class PropelMigration_1437855293
{
    public $comment = '';

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        // add the post-migration code here
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *               the keys being the datasources
     */
    public function getUpSQL()
    {
        return array (
  'main' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `Account` DROP FOREIGN KEY `Account_fk_b2f787`;

DROP INDEX `Account_fi_b2f787` ON `Account`;

ALTER TABLE `Account`

  DROP PRIMARY KEY,

  DROP `accountId`,

  ADD PRIMARY KEY (`id`);

ALTER TABLE `LegalEntityHasAccount` ADD CONSTRAINT `LegalEntityHasAccount_fk_a42fcc`
    FOREIGN KEY (`accountId`)
    REFERENCES `Account` (`id`)
    ON UPDATE CASCADE
    ON DELETE CASCADE;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *               the keys being the datasources
     */
    public function getDownSQL()
    {
        return array (
  'main' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

ALTER TABLE `Account`

  DROP PRIMARY KEY,

  ADD `accountId` INTEGER NOT NULL AFTER `bankId`,

  ADD PRIMARY KEY (`id`,`accountId`);

CREATE INDEX `Account_fi_b2f787` ON `Account` (`accountId`);

ALTER TABLE `Account` ADD CONSTRAINT `Account_fk_b2f787`
    FOREIGN KEY (`accountId`)
    REFERENCES `LegalEntityHasAccount` (`accountId`)
    ON DELETE CASCADE;

ALTER TABLE `LegalEntityHasAccount` DROP FOREIGN KEY `LegalEntityHasAccount_fk_a42fcc`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
',
);
    }

}