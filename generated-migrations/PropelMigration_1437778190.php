<?php

/**
 * Data object containing the SQL and PHP code to migrate the database
 * up to version 1437778190.
 * Generated on 2015-07-25 01:49:50 
 */
class PropelMigration_1437778190
{

    public $comment = '';

    public function preUp($manager)
    {
        // add the pre-migration code here
    }

    public function postUp($manager)
    {
        $manager->getAdapterConnection('main')->exec("

            INSERT IGNORE INTO `LegalEntityType` (`type`) VALUES
                	('Bank'),
                	('Plant');

            INSERT IGNORE INTO `Currency` (`code`) VALUES
            	   ('ECON');
            
            ");
    }

    public function preDown($manager)
    {
        // add the pre-migration code here
    }

    public function postDown($manager)
    {
        // add the post-migration code here
    }

    /**
     * Get the SQL statements for the Up migration
     *
     * @return array list of the SQL strings to execute for the Up migration
     *         the keys being the datasources
     */
    public function getUpSQL()
    {
        return array(
            'main' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

CREATE TABLE `LegalEntityType`
(
    `type` VARCHAR(64) NOT NULL,
    `baseTax` int(11) unsigned NOT NULL,
    PRIMARY KEY (`type`)
) ENGINE=InnoDB;

CREATE TABLE `LegalEntity`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(255) NOT NULL,
    `type` VARCHAR(64) NOT NULL,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`id`),
    INDEX `LegalEntity_fi_616fab` (`type`),
    CONSTRAINT `LegalEntity_fk_616fab`
        FOREIGN KEY (`type`)
        REFERENCES `LegalEntityType` (`type`)
) ENGINE=InnoDB;

CREATE TABLE `Bank`
(
    `id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `Bank_fk_0ebfd8`
        FOREIGN KEY (`id`)
        REFERENCES `LegalEntity` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `Plant`
(
    `id` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    CONSTRAINT `Plant_fk_0ebfd8`
        FOREIGN KEY (`id`)
        REFERENCES `LegalEntity` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `Currency`
(
    `code` VARCHAR(4) NOT NULL,
    PRIMARY KEY (`code`)
) ENGINE=InnoDB;

CREATE TABLE `Account`
(
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `amount` int(11) unsigned DEFAULT 0 NOT NULL,
    `currencyCode` VARCHAR(4) NOT NULL,
    `bankId` INTEGER NOT NULL,
    PRIMARY KEY (`id`),
    INDEX `Account_fi_50666e` (`currencyCode`),
    INDEX `Account_fi_b14824` (`bankId`),
    CONSTRAINT `Account_fk_50666e`
        FOREIGN KEY (`currencyCode`)
        REFERENCES `Currency` (`code`),
    CONSTRAINT `Account_fk_b14824`
        FOREIGN KEY (`bankId`)
        REFERENCES `Bank` (`id`)
) ENGINE=InnoDB;

CREATE TABLE `LegalEntityHasAccount`
(
    `accountId` INTEGER NOT NULL,
    `legalEntityId` INTEGER NOT NULL,
    PRIMARY KEY (`accountId`),
    INDEX `LegalEntityHasAccount_fi_279ea5` (`legalEntityId`),
    CONSTRAINT `LegalEntityHasAccount_fk_279ea5`
        FOREIGN KEY (`legalEntityId`)
        REFERENCES `LegalEntity` (`id`)
) ENGINE=InnoDB;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
'
        );
    }

    /**
     * Get the SQL statements for the Down migration
     *
     * @return array list of the SQL strings to execute for the Down migration
     *         the keys being the datasources
     */
    public function getDownSQL()
    {
        return array(
            'main' => '
# This is a fix for InnoDB in MySQL >= 4.1.x
# It "suspends judgement" for fkey relationships until are tables are set.
SET FOREIGN_KEY_CHECKS = 0;

DROP TABLE IF EXISTS `LegalEntityType`;

DROP TABLE IF EXISTS `LegalEntity`;

DROP TABLE IF EXISTS `Bank`;

DROP TABLE IF EXISTS `Plant`;

DROP TABLE IF EXISTS `Currency`;

DROP TABLE IF EXISTS `Account`;

DROP TABLE IF EXISTS `LegalEntityHasAccount`;

# This restores the fkey checks, after having unset them earlier
SET FOREIGN_KEY_CHECKS = 1;
'
        );
    }
}